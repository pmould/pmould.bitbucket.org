/**
*
*	Employee Profiles App
*
*	@fileOverview Main Script for Employee Profiles App using  the PaulProfileObject
*
*	@author Paul A. Mould
*
*
*
*	@summary Application to show how OOP works in JavaScript
*
*	@see PaulProfileObject() class in displayProfile.js; render() instance member of PaulProfileObject class in displayProfile.js
*
*	Try using different arguments in the PaulProfileObject.renderPaul() function
*
*/


var PaulBioText = PaulProfileObject.render(true, "lightblue", true, PaulProfileObject.callBackFunction);

$(".displayBioBtn").click (function(){
		alert(PaulBioText);
});
