
/**
*   Employeee Profiles App
*
*
*   Try configuring or extending the PaulProfileObject to add different functionality
*   And see if you can make a class in javascript to dynamically create ProfileObjects using the
*
*   @author Paul A. Mould
*
*
*/

/**
*   PaulProfileObject instance
*
*
*   @namespace PaulProfileObject
*   @type {Object}
*   @property {String}   names                -  Employee Name
*   @property {String}   profilePicture       -  URL String to a profile picture
*   @property {string}   practiceArea         -  Employee practice area
*   @property {String}   officeLocation       -  Employee office location
*   @property {Array}    projects             -  List of active projects of the Employee
*   @property {String}   email                -  List of emails
*   @property {String}   yammerUrl            -  A link to Users external website profile
*/

var PaulProfileObject = {
        name: "Paul Mould",
        profilePicture: "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/7/005/08d/273/3cf311f.jpg",
        practiceArea: "Java, JavaScript, PHP",
        officeLocation: "Mobile",
        projects: ["iOS App", "Training Resources Project", "Bench Competition Project"],
        email: "paul.mould@ruralsourcing.com",
        yammerUrl: "https://www.linkedin.com/in/paulmould",
        /**
        *  @function callBackFunction
        *  @description callback function to be executed when the User Profile Renders to the app
        *  @memberof PaulProfileObject
        *  @instance
        *
        *
        */
        callBackFunction: function() {
            alert("Paul's profile is displayed on the page");
        },

        /**
        *

        *   @function
        *   @memberof PaulProfileObject
        *
        *   @description Renders the User Profile's HTML to the Screen with configuration options
        *
        *   @instance
        *   @param {Boolean} displayPicture
        *       If set to true it would display the Profile Picture
        *   @param {String} backgroudColor
        *       This string represents the backgroundColor of the Page
        *       If incorrect color is entered the backgound is white
        *   @param {Boolean} displayBio
        *       If set to true it would display the  Bio information
        *   @param {Function} callBackFunction
        *       A function to call once the the profile has finished rendering to the Page
        *   @return {String}
        *       Returns a string containing a bio description of the User
        *
        */
        render: function(displayPicture, backgroudColor, displayBio, callBackFunction) {

                htmlProfileBlock= " <div class='container-fluid profile'> <div class='row'><div class='col-md-10 col-md-offset-1'> ";

                //Display Profile Picture Thumbnail if displayPicture is true
                if (displayPicture == true) {
                    profileImg = "<img src='" + this.profilePicture + "'/>";
                    imgThumbNail = "<div class='row'><div class='col-xs-4 col-md-2'><a href='"+this.yammerUrl+"' target='_blank' class='thumbnail'>"+profileImg+"</a></div></div>";
                    htmlProfileBlock += imgThumbNail;
                }

                // Add Profile Name in Title
                htmlProfileBlock += "<h1>"+this.name+"</h1>"; //Display Name

                // Add Display Bio Button
                htmlProfileBlock += "<button class='displayBioBtn btn btn-default'><span class='glyphicon glyphicon-user'></span> Display Bio</button> ";

                //Display Bio if displayBio is true
                 if (displayBio == true)
                {
                    bioPanelHeading = "<div class='panel-heading'><h3 class='panel-title'>Bio</h3></div>";
                    bioPanelBody = "<div class='panel-body'>"+this.toString()+"</div>";
                    htmlProfileBlock += "<div class='panel panel-default'>"+bioPanelHeading+bioPanelBody+"</div>";
                }

                // Changes the background color of his profile page
                // Removed $(".profile").addClass(backgroudColor);
                // Refactored the application to use JQuery funcion .css() to change the profile background
                $('.profile').css("background-color", backgroudColor);

                // Creates the Project List HTML
                projects = this.projects;
                for (i = 0; i<this.projects.length; i++) {
                    var projectsList = projectsList || " "; // initializes to empty string if undefined
                    projectsList += "<li class='list-group-item'>" + projects[i] + "</li>";
                }

                // Creates the Panel the Projects List and inserts the projectsList in the panel body
                projectsPanelHeading = "<div class='panel-heading'><h3 class='panel-title'>Active Projects</h3></div>";
                projectsPanelBody = "<div class='panel-body'><ul>"+projectsList+"</ul></div>"; // Project List is inserted into the Boostrap Panel

                 // Add the Project List Panel
                htmlProfileBlock += "<div class='panel panel-default'>"+projectsPanelHeading+projectsPanelBody+"</div>";

                //Add Contact Information
                htmlProfileBlock += "<p class='contact' >Contact "+this.name.split(' ')[0]+" at <a href='mailto:"+this.email+"'>"+this.email+"</a></p>";
                htmlProfileBlock += "</div><!-- END OF COLUMN --></div><!-- END OF ROW --></div><!-- END OF FLUID CONTAINER -->";

                //Render Profile HTML Block to the DOM
                $("#profilesWrapper").html(htmlProfileBlock); //Appends the created  .profile div block to the DOM


                // Change the Background of the profile
                $('.profile').css("background-color", backgroudColor);


                callBackFunction();  //Executes the callback function at the end of the function



                return this.toString();  //Returns the Bio information
              },
        /**
        *   @function
        *   @memberof PaulProfileObject
        *   @instance
        *   @description Returns the bio description of the User Profile
        *   @return {String} A string containing the bio description of the profile
        */
        toString: function() {

                        var bio = "Hi! My name is " + this.name + ". My practice area is in " + this.practiceArea + " and I work in the " + this.officeLocation + " RSI Office.";

                        return bio;
                    }
        };
